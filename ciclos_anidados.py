

"""
n = 0
while n < 10:
    print(n)
    n += 1
"""

numeros = [1,2,3]
indice = 0
#Itera la lista de números
#For padre (for/ciclo principal)
for n in numeros:
    print("-------------")
    #Iteración 10 veces
    #For hijo (for/ciclo anidado)
    for i in range(0, 10):
        print(indice, "valor-> ", n)
    indice += 1

"""
LINK DE LO VISTO HASTA EL MOMENTO:
https://paste.ofcode.org/96Duc8GPLd4qm4d3nWgkci
"""
"""
print("************************************\n")
for j in range(0, 10):
    for n in numeros:
        print(n)

print("************************************\n")

#Variable utilizada para el ciclo padre
h = 0
#Ciclo padre
while h < 10:
    #Variable utilizada para el ciclo hijo
    j = 0
    #ciclo hijo
    while j < 10:
        print(j)
        j += 1
    h += 1
"""
"""
Ejercicio 1:
Solicitar N notas de 3 estudiantes y 
sumar las notas pertenecientes a cada estudiante
"""
"""
#Paulo cesar
# Importo librería del sistema
from os import system

#Borra pantalla
system("cls")

cant_e = int(input("Cuántos estudinates?: "))
cant_n = int(input("Cuántas notas por estudinate?: "))

estudiantes = range(1, cant_e+1)



#Itera la lista de números
#For padre (for/ciclo principal)
for n in estudiantes:
    print("---------------")
    #Iterción 10 veces
    #For hijo (for/ciclo anidado)
    for i in range(1, cant_n+1):
        i = float(input("N{} de E{}: ".format(n, i)))
"""
"""
#Rubén Antonio
for k in range(3):
    cantNotas:int =int(input ("Ingrese la cantidad de notas"))
    c: int=1
    sumaNotas:float=0
    while c <= cantNotas:
        Nota:float= int(input("Ingrese la nota",c))
        c =+1
        SumaNotas=+Nota

        promedioNotas:float =sumaNotas/cantNotas
    print ("El promedio de notas es:  $, ",end=" ")
    print (promedioNotas)
"""
"""
Ejercicio 2:
Escalar la solución anterior a:
*Retornar el promedio de notas de cada estudiante
"""


"""
Ejercicio 3:
Escalar las soluciones a:
Solicitar por teclado a cuantos estudiantes 
se les ingresará las notas
"""

#Juan Manuel
numero_estudiantes = int(input("A cuantos estudiantes se les ingesara notas: "))
numero_notas = 3


def suma():
    i = 1
    while i <= numero_estudiantes:
        estudiante = input(f"El esudiate {i}: ")
        num = 1
        resultado = 0
        
        while num <= numero_notas:
            nota = float(input(f"nota {num}: "))
            resultado += nota
            num += 1
            
        print("Suma de las notas: ", resultado)
        resultado = resultado / numero_notas
        print(f"Promedio de {estudiante} es: ",resultado)
        
        i += 1
    
suma()




#Ovidio
"""
x = int(input('Notas de cuantos estudiantes: '))
#Itera hasta x+1 donde exluye el última unidad
for n in range(1, x+1):
    respuesta = 'y'
    sumatoria: float = 0
    #Variable que me cuenta las iteraciones del while
    contador = 0
    #Itera mientras la respuesta sea 'y'
    while respuesta == 'y':
        #Solicitamos las notas del estudiante y
        #casteamos (convertimos) a flotante con el float(input(""))
        nota_estudiante:float = float(input('Nota de estudiante{}: '.format(n)))
        sumatoria += nota_estudiante
        respuesta = input('Quiere ingresar mas notas? Digite y(yes) o n(not): ')
        contador += 1
    print('estudiante', n, "sumatoria de notas", sumatoria)
    #Obtenemos el promedio de la nota
    promedio = sumatoria/contador
    print("Promedio de nota del estudiante {} : {}".format(n, promedio))
    
    nota_estudiante = 0
"""