

#Crear lista de elementos del 0 al 9 
#(el último elemento lo exluye)
"""
numeros = range(10000)

#Iteramos los elementos anteriores
for n in numeros:
    print(n)

print("Fin del for")
print("---------------------------")
nombres = ["Pedro", "Juan", "Martha", 2, True, 3.5]

#Acceder a un posición
nombres[1] = 'Angel'
print( nombres )
"""
#Recorrer la lista de dos en dos
"""
index = 0
for valor in nombres:
    if index%2 ==0:
        print(valor)
    index += 1

"""

#mi_lista[posición] = nuevo_valor
"""
Ejercicio:
Desarrolle un algoritmo por medio de funciones 
que permita actualizar las notas de un estudiante almacenadas 
en una lista. La función a desarrollar debe de obtener 
como parámetro dicha lista. 
Debemos de pedir al usuario las nuevas notas del estudiante
"""
"""
#Construimos nuestra función
def actualiza_notas(notas_est):
    index = 0
    #Iteramos la lista
    for n in notas_est:
        #Solicitamos el valor de la nota
        nueva_nota = float( input("por favor ingrese nueva nota: ") )
        notas_est[index] = nueva_nota
        #Actualizamos
        index += 1

    return notas_est

#Creamos la lista con notas iniciales
lista_notas = [3.2, 4, 5]
#Llamamos la función
actualiza_notas(lista_notas)
"""
"""
#Creamos un listado de números del 50 a 199
numeros = range(50, 200)
#[50, 51, 52, 53, ...]
#Iteramos la lista de números
for n in numeros:
    #Verificamos que el número sea par
    #print(n)
    
    if n%2==0:
        print(n)
"""
"""
#Lista de números 
numeros = range(200, 50, -1)
# [200, 199, 198, ...51]

for n in numeros:
    print(n)
"""

"""
LINK CÓDIGO:
https://paste.ofcode.org/QjDrgxxh54D8VjTzzbVTUw
"""

"""
apellidos = ['Perez', 'Quintero', 'Smith']
#Variable representativa del indice de mi lista
index = 0
for cadena in apellidos:
    print("valor: ",cadena," indice: ", index)
    index = index + 1

"""
"""
for n in range(0, 10000):
    print(n)
"""

contador = 0
while contador < 10000:
    print(contador)
    contador += 1