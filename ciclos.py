
numero = 0

#Actualización de variable
"""
numero = numero + 2
numero -=  2
"""
#break -> romper ciclo
#continue -> volver al inicio del ciclo
#Ciclo while, mostrar números del 0 al 1000
"""
while numero <= 1000:
    print(numero)
    numero += 1
"""

while numero <= 3:
    print(numero)
    numero += 1
    if numero == 2:
        #break -> #Rompe el ciclo
        continue #Regresa al inicio del ciclo
    


"""
Ejercicio 1:
Desarrolle una función que retorne la sumatoria
de los números de 1 al 1000
1+2+3+4+5+6+7+8+9...
"""
#Pedro Manuel
numero = 1
while numero <= 1000:
    print(numero)
    numero += 1

#Ovidio
def sumatoria():
    num = 1
    sumatoria = 0
    while num <= 1000:
        sumatoria += num
        num += 1
    print(sumatoria)

sumatoria()

#Paulo cesar
def sumatoria():
    suma = 0
    num = 1
    while num <= 1000:
        print(num)
        num += 1
        suma = suma + num
    print(suma)
sumatoria()

#Mauricio
def suma():
    num = 1
    #suma = 0
    resultado = 0
    while num <= 1000:
        resultado += num
        num += 1
    return resultado
print("La suma es: ", suma())

#Miguel Angel
def sumatoria():
    a=1
    b=0
    while a <= 1000:
        a += 1
        b += a 
    return b

print(sumatoria())

"""
Ejercicio 2:
Desarrolle un programa que imprima la suma de los términos 
impares comprendidos entre 50 y 200. Debe mostrar la fórmula
Ejemplo: suma = 51+53+55.....199
"""
#Mauricio
def suma():
    
    numero = 50
    while numero <= 200:
        
        if(numero % 2 != 0):
            print("+")
            print(numero)
            
        numero += 1

suma()

#Juan Manuel
def suma():
    
    numero = 50
    while numero <= 200:
        if numero % 2 != 0:
            print("+")
            print(numero)
            
        numero += 1


suma()

#Miguel Ricardo
numeroimp = 51
print("La suma es: ", numeroimp, end='+')
while numeroimp <= 200:
    numeroimp+=2
    print(numeroimp, end=" + ")
    if numeroimp == 199:
        print(numeroimp)
        break

#---------
numeroimp = 51
resultado = "La suma es: "+str(numeroimp)+" + "
while numeroimp <= 200:
    numeroimp+=2
    resultado += str(numeroimp)+" + "
    if numeroimp == 199:
        resultado += str(numeroimp)+" + "
        break
print(resultado)

"""
Ejercicio 3:
Desarrolle un programa que pida contiene cinco notas 
de un estudiante.
Retorne el promedio de las notas
"""
#Mauricio
def promedio():
    cont = 1
    num = 0
    suma = 0
    while cont < 5:
        num = float(input("Por favor ingrese su nota: "))
        suma = suma + num
        cont += 1
    resultado = float(suma/float(cont-1))
    return resultado

print ("El promedio de notas es: ", promedio())

#Miguel Angel
try:
    def notas():
        a=1
        nota=0
        suma=0
        while a<=5:
            nota=int(input("Por favor ingrese su nota: "))
            suma+=nota
            a+=1
            if a==5:
                promedio = suma / a
        return promedio

    print(notas())
except:
    exit()

#Ovidio
def promedio():
    promedio = 0
    c1 = 1
    while True and c1 <= 5:
        notac1 = int(input('Nota C1: '))
        promedio += notac1 / c1
        c1 +=1
    return promedio

print(promedio())
#result_promedio = promedio()

#print(result_promedio)

#Nelson

def average(nMin:int, nMax:int, increase: int)->float:
    number:int = 0
    score: float = 0.0
    average: float = 0.0
    while(number < nMax):
        score += float(input("Por favor ingrese su nota {}: ".format(number+1)))
        number += increase
    average = score/number
    return average
        

MIN: int = 1
MAX: int = 10
INC: int = 1

#Ejercicio 3
print("Nota promedio: {}".format(average(MIN,MAX,INC)))

#Juan Manuel
def promedio():
    num_notas = int(input("Cuantas notas promediaras?: "))
    a = 1
    promedio = 0
    while a <= num_notas:
        notas= float(input(f"{a} Nota: "))
        
        a += 1
        promedio += notas / num_notas
    print("Tu promedio de notas es de: ", promedio)


promedio()

#Renata
def  prom (): 
    cantnotas  =  float ( input ( "Indique cuantas notas va ingresar:" )) 
    cont = 0
    num  =  0 
    suma  =  0 
    while  cont  <  cantnotas : 
        num  =  float ( input ( "Por favor ingrese su nota:" )) 
        suma  =  suma  +  num 
        cont  +=  1 
    resultado  =  float ( suma / cont ) 
    return  resultado 
print  ( "El promedio de notas es:" ,  prom ())


#Mauricio
def promedio():
    cont = 1
    num = 0
    suma = 0
    aux = 1
    while True:
        num = float(input("Por favor ingrese su nota: "))
        suma = suma + num
        cont += 1
        aux = float(input("Marque 0 para ver el promedio o cualquier número para continuar: "))
        if aux == 0:
            break
    resultado = float(suma/float(cont-1))
    return resultado
print ("El promedio de notas es: ", promedio())