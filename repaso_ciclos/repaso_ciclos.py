

"""
Solicitar al usuario las N ventas generadas,
posteriormente imprimir el promedio
"""

"""-------WHILE-------"""
"""
#Variable que almacena la sumatoria de las ventas
ventas: float = 0
#Variable que almacena la opción ingresada por el usuario (-1 parar el ciclo)
opc = 0
#Variable que me representa un contador para calcular el promedio
cont = 0
while opc != -1:
    ventas += float( input("Por favor ingrese las ventas generadas: ") )
    cont += 1
    opc = int( input("¿Desea continuar? -1 (No) 0(si)") )

#Calculamos el promedio
promedio = ventas/cont
#Imprimimos el promedio
print("Promedio de ventas: ", promedio)
"""

"""--------FOR--------"""
#Solicitamos el número de itaraciones a realizar en el for
num_ventas = int( input("Ingrese el número de ventas a digitar: ") )
#Creamos una lista de tamaño 'num_ventas'
rango_iteracion = range(num_ventas)
ventas = 0
#Creamos el for
for _ in rango_iteracion:
    ventas += float( input("Por favor ingrese las ventas generadas: ") )
#Calcular el promedio
if num_ventas == 0:
    print("No hay promedio para mostrar")
else:
    promedio = ventas/num_ventas
    print("El promedio de ventas es: "+str(promedio))


