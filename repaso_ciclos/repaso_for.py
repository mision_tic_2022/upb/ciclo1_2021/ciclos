"""
De un listado de alumnos, imprimir los
que se enuentran en una posición impar, con el siguiente formato:
nombre1 - nombre2 - ...
"""
#Listado de los nombres de estudiantes
alumnos = ["juan", "pedro", "andres", "martha", "luisa"]
#Variable que representa el índice de la lista durante la interación
index = 0
nombres_concatenados = ""
#Iteramos la lista de alumnos
for nombre in alumnos:
    if index%2 != 0:
        nombres_concatenados += nombre +" - "
    index += 1

print(nombres_concatenados)