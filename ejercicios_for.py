

"""
Ejercicio 1:
Solicite al usuario un rango de números para 
mostrar en pantalla saltando de 3 en 3 
(desde - hasta)
"""
#Renata
inicio=int(input("ingrese el numero de inicio del conteo"))
fin=int(input("ingrese el numero de fin del conteo"))
for n in range(inicio,fin,3):
    print(n)

x: int = int(input('Digite inicio del rango: '))
y: int = int(input('Digite fin del rango: '))
def rango (x, y):
    
    for p in range(x, y, 3):
        print(p)
rango(x,y)


#------------------------------------------

#Juan manuel
rango_desde = int(input("Ingrese un rango de numeros, desde: "))
rango_hasta = int(input("hasta: "))

rango = range(rango_desde,rango_hasta + 1,3)

for x in rango:
    print(x)

#------------------------------------------

#Ruben dario
rango_desde = int(input("Ingrese un rango de numeros, desde: "))
rango_hasta = int(input("hasta: "))

rango = range(rango_desde,rango_hasta + 3,3)

for x in rango:
    print(x)

#------------------------------------------

desde=int(input("ingrese el numero desde el cual desea ver los datos: "))
hasta=int(input("ingrese los numeros hasta el cual desea ver los datos: "))
numero=range(desde,hasta,3)
for n in numero:
    print(n)

#------------------------------------------

#Mauricio Hernando
inf = int(input("Por favor ingrese el límite inferior del intervalo: "))
sup = int(input("Por favor ingrese el límite superior del intervalo: "))
if inf > sup:
    aux = inf
    inf = sup
    sup = aux 
rango = range(inf,sup,3)

for i in rango:
    print (i)

#------------------------------------------

#Paulo cesar
def mostrar(inicio,fin):
    numeros = range(inicio,fin,3)
    for n in numeros: 
        print(n)
    return numeros


a = int (input ("Desde qué número: "))
b = int (input ("Hasta qué número: "))

mostrar(a,b)

#Miguel ricardo
print("A continuación, ingrese los números para crear un rango: ")
n1 = int(input("Ingrese numero inicial: "))
n2 = int(input("Ingrese número final: "))
numeros = range(n1,n2,3)
print("------------------------- \n Los números serán: ")
for n in numeros:
    print(n,end = ' ')
"""
Ejercicio 2:
Solicite al usuario un número positivo y muestre en pantalla
todos los números que van desde el número ingresado 
hasta el doble de si mismo
"""
#Juan manuel
num_positivo = int(input("Ingrese un numero positivo: "))

rango = range(num_positivo,num_positivo * 2 + 1)

for x in rango:
    
    print(x)

#------------------------------------------

#Renata
numero=int (input("ingrese un numero positivo: "))
if numero > 0:
    doblenumero=2*numero
    for n in range(numero,doblenumero):
        print(n)
else:
    print ("no es positivo")

#------------------------------------------
#Mauricio Hernando
inf = int(input("Por favor ingrese un número entero mayor a 0: "))
sup = 2 * inf
rango = range(inf,sup+1)
for i in rango:
    print(i)

#------------------------------------------

#Mauricio Andres
num1=int(input("Por favor ingrese un numero positivo: "))
num2=0
if num1>0:
    num2=num1*2
for n in range(num1,num2):
    print(n)
"""
Ejercicio 3:
Desarrolle un FUNCION que retorne la sumatoria de los números del 
1 al 1000 (usando for)
"""
#Juan Manuel
def sumatoria():
    contador = 0
    rango = range(1,1001)
    for suma in rango:
        contador = contador + suma

    print(contador)

sumatoria()

#------------------------------------------

#Renata
#ejercicio 3
def sumatoria():
    suma = 0
    for num in range (0,1001):
        print(num)
        suma = suma + num
    print(suma)
sumatoria()



def sumatoria ():
    #Lista de números 
    numeros = range(1000, 0, -1)
    suma=0
    for n in numeros:
        suma = n + suma
        
    return n

print( sumatoria() )

"""
Todas las soluciones:
https://paste.ofcode.org/AxhxipByzfgJVBUNn2ZGy5
"""